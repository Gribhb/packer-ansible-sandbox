packer {
  required_plugins {
    amazon = {
      version = ">= 1.1.5"
      source  = "github.com/hashicorp/amazon"
    }
  }
}


#### VARIABLES 	####
####################
variable "ssh_key_public_path" {
  type    = string
  default = "/tmp/ansible.pub"
}

variable "ssh_key_private_path" {
  type    = string
  default = "/tmp/ansible"
}


#### DATA SOURCE ####
#####################
data "amazon-ami" "debian11" {
  filters = {
    name                = "debian-11-amd64-*"
    root-device-type    = "ebs"
    virtualization-type = "hvm"
  }
  max_retries = "2"
  most_recent = true
  owners      = ["136693071363"]
}

source "amazon-ebs" "ansible-controller" {
  ami_name      = "ansible-controller"
  instance_type = "t2.micro"
  region        = "eu-west-3"
  source_ami    = "${data.amazon-ami.debian11.id}"
  ssh_username  = "admin"
}

source "amazon-ebs" "ansible-host" {
  ami_name      = "ansible-host"
  instance_type = "t2.micro"
  region        = "eu-west-3"
  source_ami    = "${data.amazon-ami.debian11.id}"
  ssh_username  = "admin"
}


#### BUILDS ####
################
build {
  name = "lab-ansible"
  sources = [
    "source.amazon-ebs.ansible-controller",
    "source.amazon-ebs.ansible-host"
  ]

  provisioner "file" {
    source      = "${var.ssh_key_private_path}"
    destination = "/tmp/ansible"
    only        = ["amazon-ebs.ansible-controller"]
  }

  provisioner "file" {
    source      = "${var.ssh_key_public_path}"
    destination = "/tmp/ansible.pub"
    only        = ["amazon-ebs.ansible-host"]
  }

  provisioner "shell" {
    only = ["amazon-ebs.ansible-controller"]
    scripts = [
      "scripts/common.sh",
      "scripts/controller.sh"
    ]
  }

  provisioner "shell" {
    only = ["amazon-ebs.ansible-host"]
    scripts = [
      "scripts/common.sh",
      "scripts/host.sh"
    ]
  }
}
