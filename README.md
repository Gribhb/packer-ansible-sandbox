1. Clone project and `cd packer-ansible-sandbox`
`
2. Create ssh key pair in the /tmp directory called **ansible** or change the default value of `ssh_key_public_path` and `ssh_key_private_path` in the [lab-ansible-aws.pkr.hcl](/lab-ansible-aws.pkr.hcl)

3. `packer init .` make sure to be in the repo directory (`packer-ansible-sandbox`)
`
3. `packer build .`

4. Usage these ami by create 2 ec2 (1 with ami controller and 1 or more with host controller)

> Make sure to set right security group to ensure the ssh communication between your instances (depending of your infrastructure)
