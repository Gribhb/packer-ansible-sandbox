#!/bin/bash

sudo chown ansible:ansible /tmp/ansible.pub
sudo -u ansible mv /tmp/ansible.pub /home/ansible/.ssh/authorized_keys
