#!/bin/bash

sudo apt install -y vim git ansible
sudo chown ansible:ansible /tmp/ansible
sudo -u ansible mv /tmp/ansible /home/ansible/.ssh/id_rsa
sudo -u ansible chmod 600 /home/ansible/.ssh/id_rsa
