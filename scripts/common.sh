#!/bin/bash

sudo useradd -m ansible
sudo -u ansible mkdir /home/ansible/.ssh
sudo sh -c "echo 'ansible ALL=NOPASSWD: ALL' >> /etc/sudoers"
sudo apt update
sudo apt install -y python3
